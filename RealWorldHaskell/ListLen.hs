
listLen :: [a] -> Int

listLen xs = let llen (x:xxs) n = llen xxs (n+1)
                 llen []    n = n
             in llen xs 0 


listLen2 :: [a] -> Int
listLen2 xs = llen xs 0
                where llen (x:xxs) n = llen xxs (n+1)
                      llen []      n = n

